# Static Info
- Native PCCE
    - The original PCCE which does not use SCCs
- APCCE
    - Short for "run PCCE on the SCC graph"
- ASCCE
    - Short for "run SCCE on the SCC graph"
- Cycle edges
    - Edges inside SCCs


Bench | Cyclicity | Native PCCE Bits | APCCE Bits | ASCCE Bits | Total edges | Back edges | Cycle edges | SCCs | Complex SCCs | Total nodes
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
403.gcc   | Y | 108 | 34 | 58 | 38977   | 4325 | 13775 |1218 |193|2259
502.gcc_r | Y | 214 | 66 | 148 | 131388 | 5490 | 28330 |17182|459|19011
505.mcf_r | Y | 6 | 6 | 7 | 126 | 1 | 1 | 32 | 1 | 32
507.cactuBSSN_r | Y | 28 | 29 | 55 | 22820 | 53 |67|1040|12| 1048  
508.namd_r      | N | 10 | 10 | 13 | 553 | 0 | 0 | 61 | 0 | 61
510.parest_r    | Y | 25 | 25 | 42 | 15080 | 83 | 200|1234|46 | 1315
511.povray_r    | Y | 57 | 36 | 57 | 8258 | 939|2380|377|57|519
519.lbm_r       | N | 2 | 2 | 2 | 27 | 0 | 0 | 17|0|17
523.xalancbmk_r | Y | 42 | 40 | 87 | 22848 | 941 | 1566 | 3700 | 181 | 4055
525.x264_r      | Y | 18 | 18 | 27 | 2318 | 1 | 2 | 366 | 1 | 367
526.blender_r   | Y | 59 | 51 | 111 | 29673 | 285|478 |5682|147|5788
531.deepsjeng_r | Y | 15 | 15 | 21 | 573 | 12 | 12 | 87 | 3 | 87
538.imagick_r   | Y | 64 | 49 | 113 | 18864 | 252|524|807|40|915
541.leela_r     | Y | 13 | 12 | 17 | 1013 | 21 | 23 | 202 | 15 | 204
544.nab_r       | Y | 13 | 13 | 16 | 730 | 25|25|79|10|79
557.xz_r        | Y | 10 | 10 | 18 | 359 | 9 | 18 | 140 | 3 | 149



Bench     |Back Edges|Back Edge Bits|Cycle Edges|Cycle Edge Bits| 
---       | ---      | ---          |-----------|---------------| 
502.gcc_r |5490      |13            | 28330     | 15
505.mcf_r | 1        |0             |1          |1      
507.cactuBSSN_r | 53 |6             |67         |7
510.parest_r    |83  |7             |200        |8
511.povray_r    | 939|10            |2380       |12
523.xalancbmk_r | 941|10            |1566       |11
525.x264_r      |  1 |0             |2          |1
526.blender_r   | 285|9             |478        |9
531.deepsjeng_r | 12 |4             |12         |4
538.imagick_r   | 252|8             |524        |10
541.leela_r     | 21 |5             |23         |5
544.nab_r       | 25 |5             |25         |5
557.xz_r        | 9  |4             |18         |5

# Observation
- cactu
    - Note that PCCEAC requires more bits than PCCE although it considers fewer edges. At first I thought it was a bug.

Also only leela and parest have invoke cycle edges.


# Context numbers
- Parest is way off in terms of context numbers, not sure why
    - PCCE has more than 50% more contexts 
- Some of HCCE vs PCCE are different, but only minorly different
    - povray's HCCE doesn't have an empty stack at the end of the run (remaining 1 element)
- ASCCE always matches APCCE

In conclusion, SCCE has 100% consistent results with PCCE in terms of acyclic encoding, but for the cyclic encoding, one benchmark is way off, a few are slightly off. Don't think our cyclic encoding scheme is problematic, probably just a small implementation bug.

# Paper Points
DeltaPath is actually especially ineffient when paired with PCCE's cyclic encoding, because multiple words need to be pushed per back edge (at least two words, maybe 4 words for gcc) while our approach uses at most 16 bits for each cycle edge.

# Excluded
- perlbench
    - Some pops are not executed due to setjmp/longjmp
- omnetpp
    - "inidirect call promotion" pass fails 
- gcc
    - 148 bits for SCCE

# Todo
- [x] Replace Povray's O3 file to be the real O3.ll



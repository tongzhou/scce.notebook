# Exps Tracking

Instru | Histogram | Hist Detection
---    | --------- | --------------
5000   | 5000      | 6000


Bench | Base (1000) | PCCE | HCCE | WordsHistogram 
---   | ---  | ---  | ---  | ---
502.gcc_r | x
505.mcf_r | x | x | x 
507.cactuBSSN_r | Y
508.namd_r      | N
510.parest_r    | Y
511.povray_r    | Y
519.lbm_r       | N
523.xalancbmk_r | Y
525.x264_r      | Y
526.blender_r   | Y
531.deepsjeng_r | Y
538.imagick_r   | Y
541.leela_r     | Y
544.nab_r       | Y
557.xz_r        | Y

# Issue
For some reason, SCCE seems to be slightly slower than HCCE for deepsjeng, leela etc.


# Todo
- Get (optimized) histogram for remaining benches, including 403.gcc
- Get lib detection results in a table 
- Implement the acyclic bits optimization thing

# Status
- Changed detection exp HCCE key
    - Worked fine with povray and xalan, should be good for others as well
    - HCCE still runs slower than PCCE for povray
    - Doesn't magically fix parest

- ofcce-random runs in 4000. Seems to be using the old marena. The results could potentially by a little off, but shouldn't be too off.

- switched to old marena last night after observing cactu ran super slow.
    - the old marena doesn't give povray correct context numbers

507.cactuBSSN_r hasn't finished 5002

to try parest with -O0


Some benchmarks have very unmatched contexts for HCCE and PCCE

# povray
```
runs: range(0, 1)
<OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/511.povray_r//pcce-leaf/ref//i0/r0/
start time: 2018-10-10 15:50:22.848937
<OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/511.povray_r//pcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xps/results/cpu2017/511.povray_r//pcce-leaf/ref//i0/r0/; bash myrun.sh
[PCCE]                                                                                                                                                           ctxs: 0
  cvs: 0
  ctx collisions: 0
  cv collisions: 0
  max stack: 16
  final stack: 0
  hashes: 14437
  read ids: 92619409088 110742458140
  pushed: 2714798976
end time: 2018-10-10 16:17:00.845119
elapsed time: 1597.996 seconds, ret: 0

<OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/511.povray_r//fcce-leaf/ref//i0/r0/
start time: 2018-10-10 16:17:00.849745
<OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/511.povray_r//fcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xps/results/cpu2017/511.povray_r//fcce-leaf/ref//i0/r0/; bash myrun.sh
[SCCE]
  ctxs: 0
  cvs: 0
  max stack: 16
  final stack: 3
  ecce hashes: 15044
  scce hashes: 0
  read ids: 92619411808 464797603336
  pushed: 544447305
end time: 2018-10-10 16:48:07.727566
elapsed time: 1866.878 seconds, ret: 0

iter 0 takes 3464.883 seconds
```


```
runs: range(0, 1)
<OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/526.blender_r//pcce-alloc/ref//i1000/r0/
start time: 2018-10-11 02:28:57.683944                                                                                                                         <OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/526.blender_r//pcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xps/re
sults/cpu2017/526.blender_r//pcce-alloc/ref//i1000/r0/; bash myrun.sh                                                                                          
cp: cannot stat ‘../../../../inputs/all/*’: No such file or directory                                                                                          
[PCCE]                                                                                                                                                           ctxs: 0                                                                                                                                                      
  cvs: 0                                                                                                                                                         ctx collisions: 0                                                                                                                                            
  cv collisions: 0
  max stack: 64                                                                                                                                                  final stack: 0
  hashes: 13648                                                                                                                                                  read ids: 74374104 90088560                                                                                                                                  
  pushed: 1527569373
end time: 2018-10-11 02:34:02.319243                                                                                                                           elapsed time: 304.635 seconds, ret: 0                                                                                                                          
                                                                                                                                                               <OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/526.blender_r//fcce-alloc/ref//i1000/r0/
start time: 2018-10-11 02:34:02.337754                                                                                                                         <OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/526.blender_r//fcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xps/results/cpu2017/526.blender_r//fcce-alloc/ref//i1000/r0/; bash myrun.sh                                                                                          
cp: cannot stat ‘../../../../inputs/all/*’: No such file or directory                                                                                          
[SCCE]                                                                                                                                                           ctxs: 0
  cvs: 0                                                                                                                                                         max stack: 4096
  final stack: 2384
  ecce hashes: 13848
  scce hashes: 0
  read ids: 74374104 167400487456                                                                                                                              
  pushed: 125750787
end time: 2018-10-11 02:39:48.944655
elapsed time: 346.607 seconds, ret: 0
```

# parest
```
<OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/510.parest_r//pcce-alloc/ref//i1000/r0/                                                 [1048/1918]
start time: 2018-10-11 01:47:17.160387
<OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/510.parest_r//pcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xps
/results/cpu2017/510.parest_r//pcce-alloc/ref//i1000/r0/; bash myrun.sh
cp: cannot stat _../../../../inputs/all/*_: No such file or directory
[PCCE]
  ctxs: 0
  cvs: 0
  ctx collisions: 0
  cv collisions: 0
  max stack: 32
  final stack: 0
  hashes: 167587
  read ids: 1261579136 1476266420
  pushed: 91404610
end time: 2018-10-11 01:54:59.447509
elapsed time: 462.287 seconds, ret: 0

<OS>: mkdir -p /home/tzhou/projects/xps/results/cpu2017/510.parest_r//fcce-alloc/ref//i1000/r0/
start time: 2018-10-11 01:54:59.450891
<OS>: export CONTEXT_FILE=/home/tzhou/projects/xps/results/cpu2017/510.parest_r//fcce-random/ref/archive/selected_contexts.txt; cd /home/tzhou/projects/xp$
/results/cpu2017/510.parest_r//fcce-alloc/ref//i1000/r0/; bash myrun.sh
cp: cannot stat _../../../../inputs/all/*_: No such file or directory
[SCCE]
  ctxs: 0
  cvs: 0
  max stack: 8
  final stack: 0
  ecce hashes: 73358
  scce hashes: 0
  read ids: 1261579136 3866739104
  pushed: 11212382
end time: 2018-10-11 02:02:39.198409
elapsed time: 459.747 seconds, ret: 0
```
